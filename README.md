# Fichier de départ - index.html
Utilisez ce fichier de départ pour créer votre propre page de blogue.

Vous devrez modifier le contenu suivant:

1. le titre de la page
2. le titre h1 qui est affiché en entête de la page
3. Modifiez la section des logiciels pour inclure trois logiciels libres de votre choix (qui sont différents de ceux donnés en exemple dans l'énoncé). Incluez le logo du logiciel, une brève description et un hyperlien vers le dépôt du code source de ce logiciel. Attention! Ce n'est pas le site web du logiciel qui est recherché ici, mais l'emplacement de son code source (qui sera fort probablement sur GitHub, GitLab, ou une autre plateforme.)
